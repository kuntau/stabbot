class PicnicException(Exception):
    """
    This exception is expected. It's something the user can deal with.

    """
    pass


def is_number(s):
    """ Returns True if string is a number. """
    return s.replace('.', '', 1).isdigit()


def s2b(s, default):
    if not s: return default
    if s == "True": return True
    if s == "False": return False
    raise ValueError("string must be empty, True or False.")


import sys
import logging


class MyLog(object):
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    def __call__(self, *args, **kwargs): self.i(*args, **kwargs)

    def i(self, *args, **kwargs): logging.info(*args, **kwargs)

    def e(self, *args, **kwargs): logging.error(*args, **kwargs)

    def w(self, *args, **kwargs): logging.warning(*args, **kwargs)

    def d(self, *args, **kwargs): logging.debug(*args, **kwargs)


l = MyLog()
